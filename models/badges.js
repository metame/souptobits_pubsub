'use strict';

var redis = require('../lib/redis');

/* Save badges to database
  @param {Array} badges
  @param {Function} callback
*/
exports.save = function(badges, callback) {
  if (!badges.length) return callback(null, null); // checks to see if string has value, if not null err null data
  var badge = badges.pop(); // removing last item from badges array
  redis.lpush('badges', JSON.stringify(badge) function(err) {
    if (err) return callback(err, null);
    exports.save(badges, callback);  // makes function async recursive
  });
};