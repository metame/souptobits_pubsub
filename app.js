'use strict';

var express = require('express');
var app = express();
var badges = require('./controllers/badges');

// app.use uses middleware everytime a request is used, i.e. global
app.use(express.json());  // .json() is a method of express that expects to receive JSON

app.post('/', badges.save, badges.send, function(req, res){
  res.send('\ndone\n\n');
});

app.listen(8000);
