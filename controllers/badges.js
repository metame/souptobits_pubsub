'use strict';

var _ = require('underscore');
var model = require('../models/badges');

// Send badges to model to be saved
exports.save = function(req, res, next) {
  var badges = _.clone(req.body);  // .clone is underscore method for deep copy of object
  model.save(badges, function(err){
    if(err) return res.json(503, { error: true });
    next();
  });
};

// Send badges to to pub/sub socket in model
exports.send = function(req, res, next) {};